<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="">
      <div class="card text-center">
        <div class="card-header">
          QR CODE GENERATOR
        </div>
        <div class="card-body">
          <div class="mb-3 pr-5 pl-5">
              <input type="text" class="form-control" placeholder="Утга оруулна уу..." id="input" >
              <img src="generate.php?text=s" class="img-fluid" id="img">
          </div>

        </div>
        <div class="card-footer text-muted">
          <a href="https://www.facebook.com/togtokh.net">My social account</a>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    $("#input" ).keyup(function() {
    var qr_val =  $(this).val();
    if(qr_val){
      $("#img").attr("src","generate.php?text="+qr_val);
    }
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
  </body>
</html>
